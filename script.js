var vm = new Vue({
  'el': '#formWrap',
  data: {
    states: [
      "contactForm", //フォーム画面を表示
      "confirmContents",　//内容確認画面を表示
    ],
    stateIndex: 0, //現在の状態
    formItems: formItems,
    errorMessages:{
      "required": "この項目は必須です。",
      "hankakuNum": "半角数字で入力してください。",
      "emailAddress": "不正な形式です。"
    }
  },
  created() {  
  },
  computed: {
    state(){
      return this.states[this.stateIndex]
    }
  },
  methods: {
    reset(){
      for(item in this.formItems){
        this.formItems[item].value = ''
      }
    },
    validate(){

      let required = arg => {
        return !(arg == '');
      }

      let hankakuNum = arg => {
        let reg = /^[1234567890]+$/;
        let res = arg.match(reg);
        if(res == null){
          return false;
        }else{
          return true;
        }
      }

      let emailAddress = arg => {
        let reg = /^[\.!#%&\-_0-9a-zA-Z\?\/\+]+\@[!#%&\-_0-9a-z]+(\.[!#%&\-_0-9a-z]+)+$/;
        let res = arg.match(reg);
        if(res == null){
          return false;
        }else{
          return true;
        }
      }

      let isValid = arg => {
        var res = true;
        for(let item in arg){
          res = arg[item].isValid
          if(!res){ return res }
        }
        return res;
      }

      for(let item in this.formItems){
        switch(this.formItems[item].validateType){
          case "required":
            this.formItems[item].isValid = required(this.formItems[item].value)
            break;
          case "hankakuNum":
            this.formItems[item].isValid = hankakuNum(this.formItems[item].value)
            break;
          case "emailAddress":
            this.formItems[item].isValid = emailAddress(this.formItems[item].value)
            break;
          default:
            this.formItems[item].isValid = true
            break;
        }
      }

      if(isValid(this.formItems)){ this.stateIndex ++ };

    },
    sendMail(){
      axios.post('mail.php', this.formItems)
      .then(function (response) {
        location.href = './thanks.html'
      })
      .catch(function (error) {
        alert(error);
      });
    }
  }
})
