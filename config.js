/*
let formItems = {
  "formItemExample1": {
    value: "",
    isValid: true,　←ここはスルー
    validateType: null
  },
  "formItemExample2": {
    value: "",
    isValid: true,　←ここはスルー
    validateType: "required"
  }
}

validateType is allowed these string or null
"required" : not null　←必須の場合
"hankakuNum" : only numbers of Half size character　←半角数字
"emailAddress" : validate Email Address　←メールアドレス用
null : No need validate　←放置用
*/

/*
mail.php へ送信されるPOSTデータ
PHP側と変数名を合わせないといけないので注意
*/

let formItems = {
  "Genre": {
    value: "",
    isValid: true,
    validateType: null
  },
  "Name": {
    value: "",
    isValid: true,
    validateType: "required"
  },
  "PhoneNumber": {
    value: "",
    isValid: true,
    validateType: "hankakuNum"
  },
  "Email": {
    value: "",
    isValid: true,
    validateType: "emailAddress"
  },
  "Gender": {
    value: "",
    isValid: true,
    validateType: null
  },
  "Reason": {
    value: "",
    isValid: true,
    validateType: "required"
  },
  "Content": {
    value: "",
    isValid: true,
    validateType: null
  },
  "IsNotBot": {
    value: "",
    isValid: true,
    validateType: "required"
  }
}
